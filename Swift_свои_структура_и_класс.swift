//Структура 
struct Dog {
    var name: String
    var breed: String
    var age: Int
    var weight: Double
    
    func bark() {
        print("\(name) говорит 'Гав-гав!'")
    }
}
//Класс
class DogShelter {
    var dogs: [Dog] = []
    
    func addDog(_ dog: Dog) {
        dogs.append(dog)
        print("\(dog.name) успешно добавлен в приют")
    }
    
    func removeDog(_ dog: Dog) {
        if let index = dogs.firstIndex(where: { $0.name == dog.name && $0.breed == dog.breed }) {
            dogs.remove(at: index)
            print("\(dog.name) успешно удален из приюта")
        } else {
            print("\(dog.name) не найден в приюте")
        }
    }
    
    func feedDogs() {
        for dog in dogs {
            print("\(dog.name) накормлен")
        }
    }
}