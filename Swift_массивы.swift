// Создаем массив количества дней в месяцах
let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

// Создаем массив названия месяцев
let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

// Выводим количество дней в каждом месяце без названия месяца
for days in daysInMonths {
    print(days)
}

// Выводим название месяца и количество дней
for i in 0..<monthNames.count {
    print("\(monthNames[i]): \(daysInMonths[i])")
}

// Используем массив tuples для хранения значений
let monthsWithDays = [("January", 31), ("February", 28), ("March", 31), ("April", 30), ("May", 31), ("June", 30), ("July", 31), ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]

// Выводим элементы массива tuples
for month in monthsWithDays {
    print("\(month.0): \(month.1)")
}

// Выводим количество дней в обратном порядке
for days in daysInMonths.reversed() {
    print(days)
}

// Посчитаем количество дней от начала года для произвольной даты
let month = 3 // март
let day = 15 // пятнадцатое число
var daysCount = 0
for i in 0..<month-1 {
    daysCount += daysInMonths[i]
}
daysCount += day
print("Days since the beginning of the year: \(daysCount)")