let name = "Evelina"
let age = 22
let interests = ["Reading", "Running", "Traveling"]

print("Name: \(name)\nAge: \(age)")
print("Interests:")
for interest in interests {
   print("- \(interest)")
}