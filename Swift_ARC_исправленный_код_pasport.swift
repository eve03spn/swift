class Man {
    var passport: Passport?

    deinit {
        print("мужчина удален из памяти")
    }
}

class Passport {
    let man: Man

    init(man: Man) {
        self.man = man
    }

    deinit {
        print("паспорт удален из памяти")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.passport = passport

passport = nil
man = nil