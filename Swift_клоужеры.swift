// Сортировка с помощью замыкания
let numbers = [5, 1, 8, 3, 9, 4, 6, 2, 7]

let sortedForward = numbers.sorted { $0 < $1 }
print("Массив, отсортированный по возрастанию: \(sortedForward)")

let sortedBackward = numbers.sorted { $0 > $1 }
print("Массив, отсортированный по убыванию: \(sortedBackward)")

func sortFriendsByNameLength(names: [String]) -> [String] {
    names.sorted { $0.count < $1.count }
}

let friends = ["John", "Alex", "Diana", "Mary", "Steve"]
let sortedFriends = sortFriendsByNameLength(names: friends)

var dictionary = [Int: String]()

for friend in sortedFriends {
    let length = friend.count
    dictionary[length] = friend
}

for (key, value) in dictionary {
    print("\(key): \(value)")
}

//Проверка на пустоту
func checkForEmptyArrays(stringArray: [String], intArray: [Int]) {
    if stringArray.isEmpty {
        print("Массив строк пуст, добавляем значение")
        var mutableArray = stringArray
        mutableArray.append("New Value")
        print(mutableArray)
    } else {
        print("Массив строк не пуст: \(stringArray)")
    }
    
    if intArray.isEmpty {
        print("Массив чисел пуст, добавляем значение")
        var mutableArray = intArray
        mutableArray.append(1)
        print(mutableArray)
    } else {
        print("Массив чисел не пуст: \(intArray)")
    }
}

let emptyStringArray = [String]()
let notEmptyIntArray = [2, 4, 6, 8, 10]
checkForEmptyArrays(stringArray: emptyStringArray, intArray: notEmptyIntArray)

let emptyIntArray = [Int]()
let notEmptyStringArray = ["one", "two", "three"]
checkForEmptyArrays(stringArray: notEmptyStringArray, intArray: emptyIntArray)