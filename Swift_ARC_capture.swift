func someFunction() {
    var counter = 0
    
    // Создаем замыкание и захватываем переменную counter
    let closure = { [counter] in
        print("Counter is \(counter)")
    }
    
    counter = 10
    
    // Вызываем замыкание
    closure() // Выведет "Counter is 0"
}