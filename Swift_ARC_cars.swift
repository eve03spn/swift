enum CarAction {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case loadCargo(volume: Double)
    case unloadCargo(volume: Double)
}

struct Car {
    let brand: String
    let year: Int
    var trunkVolume: Double
    var isEngineStarted: Bool
    var areWindowsOpened: Bool
    var filledTrunkVolume: Double
    
    mutating func perform(action: CarAction) {
        switch action {
        case .startEngine:
            self.isEngineStarted = true
        case .stopEngine:
            self.isEngineStarted = false
        case .openWindows:
            self.areWindowsOpened = true
        case .closeWindows:
            self.areWindowsOpened = false
        case .loadCargo(let volume):
            let freeVolume = self.trunkVolume - self.filledTrunkVolume
            if volume <= freeVolume {
                self.filledTrunkVolume += volume
            } else {
                print("Not enough space in the trunk!")
            }
        case .unloadCargo(let volume):
            if volume <= self.filledTrunkVolume {
                self.filledTrunkVolume -= volume
            } else {
                print("Not enough cargo to unload!")
            }
        }
    }
}

struct Truck {
    let brand: String
    let year: Int
    var bodyVolume: Double
    var isEngineStarted: Bool
    var areWindowsOpened: Bool
    var filledBodyVolume: Double
    
    mutating func perform(action: CarAction) {
        switch action {
        case .startEngine:
            self.isEngineStarted = true
        case .stopEngine:
            self.isEngineStarted = false
        case .openWindows:
            self.areWindowsOpened = true
        case .closeWindows:
            self.areWindowsOpened = false
        case .loadCargo(let volume):
            let freeVolume = self.bodyVolume - self.filledBodyVolume
            if volume <= freeVolume {
                self.filledBodyVolume += volume
            } else {
                print("Not enough space in the body!")
            }
        case .unloadCargo(let volume):
            if volume <= self.filledBodyVolume {
                self.filledBodyVolume -= volume
            } else {
                print("Not enough cargo to unload!")
            }
        }
    }
}

var car1 = Car(brand: "Toyota", year: 2012, trunkVolume: 500, isEngineStarted: false, areWindowsOpened: false, filledTrunkVolume: 0)
var car2 = Car(brand: "Ford", year: 2018, trunkVolume: 700, isEngineStarted: false, areWindowsOpened: true, filledTrunkVolume: 0)

var truck1 = Truck(brand: "Volvo", year: 2015, bodyVolume: 5000, isEngineStarted: false, areWindowsOpened: false, filledBodyVolume: 0)
var truck2 = Truck(brand: "MAN", year: 2020, bodyVolume: 10000, isEngineStarted: true, areWindowsOpened: true, filledBodyVolume: 500)

car1.perform(action: .startEngine)
car1.perform(action: .openWindows)
car2.perform(action: .loadCargo(volume: 1024.5))
truck1.perform(action: .startEngine)
truck2.perform(action: .unloadCargo(volume: 2000))

var vehiclesDictionary = [car1: "car1", car2: "car2", truck1: "truck1", truck2: "truck2"]