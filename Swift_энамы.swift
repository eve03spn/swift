// Создаем enum с разными типами RawValue
enum Currency: String {
    case USD = "United States Dollar"
    case EUR = "Euro"
}

enum Month: Int {
    case January = 1
    case February
    case March
    case April
    case May
    case June
    case July
    case August
    case September
    case October
    case November
    case December
}

// enum для заполнения полей структуры
//пол
enum Gender {
    case Male
    case Female
}
//категория возраста (ребенок,подросток,взрослый,преклонный)
enum AgeCategory {
    case Child
    case Teenager
    case Adult
    case Senior
}
//стаж
enum Experience {
    case Beginner
    case Intermediate
    case Advanced
}

// все цвета радуги
enum RainbowColor {
    case Red
    case Orange
    case Yellow
    case Green
    case Blue
    case Indigo
    case Violet
}

// Функция для вывода содержимого enum в консоль
func printEnumCases() {
    enum Fruits {
        case Apple
        case Banana
    }
    
    enum Colors {
        case Green
        case Red
        case Blue
    }
    
    enum Planets {
        case Earth
        case Mars
        case Venus
    }
    
    let myFruit = Fruits.Apple
    let myColor = Colors.Green
    let myPlanet = Planets.Earth
    
    switch myFruit {
    case .Apple:
        print("apple", terminator: " ")
    case .Banana:
        print("banana", terminator: " ")
    }
    
    switch myColor {
    case .Green:
        print("green", terminator: " ")
    case .Red:
        print("red", terminator: " ")
    case .Blue:
        print("blue", terminator: " ")
    }
    
    switch myPlanet {
    case .Earth:
        print("earth")
    case .Mars:
        print("mars")
    case .Venus:
        print("venus")
    }
}

printEnumCases()

// Функция выставления оценок ученикам
enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func assignScore(for student: String, with score: Score) {
    var value: Int = 0
    
    switch score {
    case .A:
        value = 5
    case .B:
        value = 4
    case .C:
        value = 3
    case .D:
        value = 2
    }
    
    print("Студенту \(student) выставлена оценка \(value)")//вывод оценки ученику
}

assignScore(for: "Иванов Иван", with: .B)

// Создаем метод для вывода автомобилей, стоящих в гараже
enum Car {
    case Sedan
    case Coupe
    case SUV
}

class Garage {
    var cars: [Car] = []
    
    func printCars() {
        for car in cars {
            switch car {
            case .Sedan:
                print("Sedan")
            case .Coupe:
                print("Coupe")
            case .SUV:
                print("SUV")
            }
        }
    }
}

let myGarage = Garage()
myGarage.cars = [.Sedan, .Coupe, .SUV]
myGarage.printCars()